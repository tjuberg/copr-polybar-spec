Name: polybar
Version: 3.5.4
Release: 1%{?dist}
Summary: A fast and easy-to-use tool for creating status bars
License: MIT
URL: https://github.com/jaagr/polybar
Source0: https://github.com/jaagr/polybar/releases/download/%{version}/polybar-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  clang
BuildRequires:  i3
BuildRequires:  i3-devel
BuildRequires:  python3-sphinx
BuildRequires:  python3
BuildRequires:  pkgconfig(alsa)
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(jsoncpp)
BuildRequires:  pkgconfig(libcurl)
BuildRequires:  pkgconfig(libmpdclient)
BuildRequires:  pkgconfig(libnl-genl-3.0)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(xcb)
BuildRequires:  pkgconfig(xcb-cursor)
BuildRequires:  pkgconfig(xcb-composite)
BuildRequires:  pkgconfig(xcb-damage)
BuildRequires:  pkgconfig(xcb-ewmh)
BuildRequires:  pkgconfig(xcb-image)
BuildRequires:  pkgconfig(xcb-keysyms)
BuildRequires:  pkgconfig(xcb-proto)
BuildRequires:  pkgconfig(xcb-render)
BuildRequires:  pkgconfig(xcb-renderutil)
BuildRequires:  pkgconfig(xcb-sync)
BuildRequires:  pkgconfig(xcb-xkb)
BuildRequires:  pkgconfig(xcb-xrm)
Requires: cairo
Requires: jsoncpp
Requires: python2
Requires: xcb-proto
Requires: xcb-util-image
Requires: xcb-util-wm
Requires: xcb-util-xrm

%description
A fast and easy-to-use tool for creating status bars.

%global debug_package %{nil}

%prep
%setup -n %{name}-%{version}

%build
cmake .
%make_build

%install
install -Dm755 bin/polybar          %{buildroot}%{_bindir}/polybar
install -Dm755 bin/polybar-msg      %{buildroot}%{_bindir}/polybar-msg
install -Dm755 config               %{buildroot}%{_datadir}/doc/polybar/config
install -Dm755 contrib/bash/polybar %{buildroot}%{_datadir}/bash-completion/completions/polybar
install -Dm755 contrib/zsh/_polybar %{buildroot}%{_datadir}/zsh/site-functions/_polybar
install -Dm755 contrib/zsh/_polybar %{buildroot}%{_datadir}/zsh/site-functions/_polybar_msg
install -Dm755 doc/man/polybar.1    %{buildroot}%{_mandir}/man1/polybar.1

%post
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ] ; then
    /sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir || :
fi


%files
%{_bindir}/polybar*
%{_datadir}/bash-completion/completions/polybar
%{_datadir}/doc/polybar/config
%{_datadir}/zsh/site-functions/_polybar*
%{_mandir}/man1/polybar.*

%changelog
